import common.Animal;
import common.Cat;
import common.Dog;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * @author AllenLee
 */
public class ObjectOrientedProgramming {
    /**
     * 面向过程编程: C语言
     * 面向对象编程：C++、Java、Python ...
     *     Person ming = new Person();
     *     Person hong = new Person();
     *     Person 是一个类的名字
     *     ming 是我们定义的对象的名字
     *     new Person().var 快速创建类
     */

    /**
     * public key private key
     * 钥匙
     * ssh-keygen -t ed25519 -C "Gitee SSH Key"
     */

    // 内部类，用的较少
    static class Person {
        public String name;
        public int age;
    }

    // 内部类 Inner class
    static class Book {
        public String name;
        public String author;
        public String isbn;
        public double price;

        // 类的方法
        @Override
        public String toString() {
            return "Book info：{" +
                    "name='" + name + '\'' +
                    ", author='" + author + '\'' +
                    ", isbn='" + isbn + '\'' +
                    ", price=" + price +
                    '}';
        }
    }

    /**
     * 银行允许你存人民币或者其他外币
     * 存款，款类似于父类
     * @param animal
     */
    public static void test(Animal animal) {
        if (animal instanceof Cat) {
            ((Cat) animal).eatFish();
        } else if (animal instanceof Dog) {
            ((Dog) animal).eatBone();
        }
    }


    // static 表示这个方法是独立于当前类存在的，也就是静态的。
    // new Book() 的时候才生成了一个类，也就是动态的。
    // 动态，代码运行的时候，才会分配内存。
    // 静态，编译时已经分配好内存了，编译的时候已经需要运行了。
    // 静态方法内部用的类需要也是静态类。
    public static void main(String[] args) {
        Book book = new Book();
        Person ming = new Person();
        // 本质是在调用构造方法，相当于调用无参构造方法
        // 用了Builder之后，就会把类的默认的无参构造方法取消了，需要
        // 调用了 Dog 的构造方法
        Dog dog1 = new Dog();
        Dog dog2 = new Dog("22");
        Dog dog3 = new Dog("33", 5);
        Dog dog4 = new Dog(8);
//        book.author = "Allen";
//        System.out.println(book.author);
//
//        // Question2
//
//        Book book2 = new Book();
//        book2.name = "asd";
//        book2.author = "Allen";
//        book2.isbn = "123456";
//        book2.price = 8.9;
//        // 打印出来了book2的地址，book2这个对象在计算机内存中的地址
//        // 重写了 toString 方法之后：Book{name='asd', author='Allen', isbn='123456', price=8.9}
//        System.out.println(book2);
//        // 1. Builder来写
//        Dog ggBond = Dog.builder().name("GGBond").build();
//        // bark 是public的，所以能在类的外部使用
//        ggBond.bark();
////        ggBond.rename(); 因为是private方法不能被直接使用
//
//        // 创建Scanner对象
//        Scanner scanner = new Scanner(System.in);
//        // 打印提示
//        System.out.print("Input your name: ");
//        // 读取一行输入并获取字符串
//        String name = scanner.nextLine();
//        // 打印提示
//        System.out.print("Input the name that you want to name your puppy: ");
//        // 读取一行输入并获取字符串
//        String puppyName = scanner.nextLine();
//        ggBond.validateAndRename(name, puppyName);
//        ggBond.bark();

//        System.out.println("-----------------------------------------------");
//        System.out.println(dog1);
//        System.out.println(dog2);
//        System.out.println(dog3);
//        System.out.println(dog4);
//
//        dog1.printPosition();
//        dog2.printPosition();
//        dog3.printPosition();
//        dog4.printPosition();


        /**
         * 继承：extends关键字来实现继承
         */
        Cat cat = new Cat();
        cat.getName();
        cat.print();

        // 无法访问private方法
//        cat.name = "sss";

        System.out.println();

        // 继承有个特点，就是
        // 子类无法访问父类的private字段或者private方法。
        // 用protected修饰的字段可以被子类访问
        // 子类不会继承任何父类的构造方法
        /**
         * 只要某个class没有final修饰符，那么任何类都可以从该class继承。
         * 向上转型: 左父右子, 把一个子类类型安全地变为父类类型的赋值
         */

        ArrayList<Animal> animals = new ArrayList<>();
        Cat cat1 = new Cat();
        Animal dog = new Dog();
        animals.add(cat1);
        animals.add(dog);

        for (int i = 0; i < animals.size(); i++) {
            test(animals.get(i));
        }

//        cat1.speak();
//        dog.speak();
//        cat1.countLegs();

        // 向下转型：
//      cat1.countLegs()
//        ((Cat) cat1).countLegs();
//        dog.countLegs();
//        dog.countLegs();
        // java.lang.ClassCastException: common.Cat cannot be cast to common.Dog
//        ((Dog) cat1).eatBone();

        // countLeg 能够输出
//        ((Dog)cat1).countLife();

        // instanceof true
//        System.out.println((cat1 instanceof Cat));
        // 只有向上和向下转型，兄弟类不能互转
        // 向上是允许的，向下转型才会有可能有问题。
//        test(cat1);
//        test(dog);

        // 继承是is关系，组合是has关系。
    }
}
