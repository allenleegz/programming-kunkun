import java.util.ArrayList;
import java.util.Collections;

/**
 * @author AllenLee
 */
public class Array {
    public static void main(String[] args) {
        // 5位同学的成绩:
        int[] studentMark = new int[5];
        studentMark[0] = 68;
        studentMark[1] = 79;
        studentMark[2] = 91;
        studentMark[3] = 85;
        studentMark[4] = 62;

        String[] studentNames = new String[5];
        studentNames[0] = "Allen";
        studentNames[1] = "Bob";
        studentNames[2] = "Cathy";
        studentNames[3] = "Dooby";
        studentNames[4] = "Egg";

        // 可以用数组变量.length获取数组大小：
        System.out.println(studentNames.length);

        // 打印出来第1个学生的名字和他的成绩，要求：Allen's mark is: 68
        System.out.println(studentNames[0] + "'s mark is: " + studentMark[0]);

        // 打印出来第2个学生的名字和他的成绩，要求：Bob's "mark" is: 79 \" 用于在字符串中输出"
        System.out.println(studentNames[1] + "'s \"mark\" is: " + studentMark[1]);

        // ctrl+? Cmd+? 快速注释、取消注释

        System.out.println("------------------------------------------------------");


        /**
         * For 循环
         * int i=1; 初始化一个变量，只在for循环中起作用
         * i<=100;  表示循环终止的条件，如果为false则不继续循环，为true则循环继续
         * i++ i=i+1 表示每一次循环结束后做的事情
         */
        int sum = 0;
        for (int i = 1; i <= 100; i++) {
            sum = sum + i;
        }
        // 1+2+3+...+100
        System.out.println(sum);

        // for循环打印学生名字和成绩，要求：Allen's mark is: 68

        for (int i = 0; i < studentMark.length; i++) {
            System.out.println(studentNames[i] + "'s mark is: " + studentMark[i]);
        }

        // ctrl+c 复制 ctrl+v 粘贴 ctrl+x 剪贴 ctrl+z 回到上一步 ctrl+a 全选

        // i+=2 -> i=i+2

        // 反向遍历 :
        // for (int i = studentMark.length-1; i>=0; i--)

        for (String s : studentNames) {
            System.out.println(s);
        }

        System.out.println("-------------------------");

        // ArrayList 数组 List列表 LinkedList
        // int    -> Integer    boolean -> Boolean
        // 基础类型   封装类型
        // Java里面一切皆为类，Class Integer等封装
        int i = 100;
        Integer ii = 100;

        ArrayList<String> nameList = new ArrayList<>(5);
        nameList.add("Allen");
        nameList.add("Bob");
        nameList.add("Cat");

        for (String s : nameList) {
            System.out.println(s);
        }

        nameList.forEach(str -> System.out.println(str));

        nameList.forEach(s -> {
            System.out.println(s);
            System.out.println(s);
        });

        // lambda表达式 函数式编程
        nameList.forEach(System.out::println);

        System.out.println("-----------------");

        // 为什么不用数组？添加和删除元素的时候，会非常不方便。
        // [A][B][D][E][ ]

        // ArrayList
        // https://www.runoob.com/java/java-arraylist.html

        System.out.println(nameList.get(0));
        nameList.set(1, "Buddy");
        nameList.remove(1);
        System.out.println(nameList.size());
        Collections.sort(nameList);

    }
}