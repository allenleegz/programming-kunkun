package common;

import lombok.Data;

/**
 * @author AllenLee
 * Data 生成get、set方法以及 to_string
 */
//@Data
public class Cat extends Animal {

    String color = "None";

    public void eatFish() {
        System.out.println("I like fish");
    }

    public void countLife() {
        System.out.println("8");
    }

    // 继承Animal的speak，多态
    @Override
    public void speak() {
        System.out.println("Meow");
    }

    public void print() {
        // super关键字表示父类（超类）。子类引用父类的字段时，可以用super.fieldName。
        super.run();
        this.run();

        System.out.println(this.getMoney());
        this.setMoney(1000000);
        System.out.println(this.getMoney());
        this.setMoney(-1000000);
        System.out.println(this.getMoney());

        System.out.println(setMoney(100));
    }

}
