package common;

import lombok.Data;

/**
 * @author AllenLee
 */ // 自动生成get和set方法
@Data
public class Animal {

    public void speak() {}

    String name = "default";

    // final 一旦设置不能被更改
    private final int legs = 4;
    private int money;
    protected int age;

//    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }

    protected void run() {
        System.out.println("Running...");
    }

    public int setMoney(int money) {
        this.age = 999;
        if (money < 0) {
            return this.money;
        }
        this.money = money;
        return 666666;
    }
}
