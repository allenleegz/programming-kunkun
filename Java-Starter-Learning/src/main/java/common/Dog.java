package common;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

/**
 * @author AllenLee
 */
// @注解
@Builder
//@AllArgsConstructor
//@NoArgsConstructor
public class Dog extends Animal {

    public void eatBone() {
        System.out.println("Yummy");
    }

    public void countLife() {
        System.out.println("1");
    }

    @Override
    public void speak() {
        System.out.println("Woow");
    }

    // 类的属性 注释
    String name = "default";
    int position = 0;
    Bone bone;

    // 类的方法
    // public 此方法是否能被外部使用
    // private 不能被外部使用
    // void 返回类型，void 表示没有返回值，String int double
    public void bark() {
        System.out.println("My name is " + name + ", Wow Wow!");
    }

    /**
     * 判断是不是狗的主人，是的话允许重命名，否则不允许
     * @param name
     * @param puppyName
     */
    public void validateAndRename(String name, String puppyName) {
        if (name.equalsIgnoreCase("Allen")){
            System.out.println(rename(puppyName));
        } else {
            System.out.println("You are not allowed to rename the pet.");
        }
    }

    public void printPosition() {

        String str = "D-O-G" + this.name;

        for (int i = 0; i < this.position; i++) {
            System.out.print(" ");
        }
        System.out.println(str);
        System.out.println("___________________________________");
    }

    private String rename(String name) {
       this.name = name;
       return "My name has been changed to :" + this.name;
    }

    // 同名方法 重载
    private String rename() {
        this.name = "旺财";
        return "My name has been changed to :" + this.name;
    }

    // 需要手动写无参构造方法出来
    public Dog() {
    }

    // 有参构造方法 Overload
    public Dog(String name) {
        this.name = name;
    }

    public Dog(String name, int position) {
        this.name = name;
        this.position = position;
    }

    public Dog(int position) {
        this.name = "None";
        this.position = position;
    }

    // 重写
    @Override
    public String toString() {
        return "Dog{" +
                "name='" + name + '\'' +
                ", position=" + position +
                '}';
    }
}
