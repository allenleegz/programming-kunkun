#include <iostream>

using namespace std;

// Function prototypes
void print1to9(int n);
void print10to19(int n);
void print20to99(int n);

int main() {
    int num;
    cout << "Enter a number in Range [1--19].\n";
    cin >> num;
    if (num == 0) {
		cout << "0 is not in range from 1 to 99" << endl;
	} 
    if (num >= 1 && num <= 9) {
        print1to9(num);
    }
    // add your logic here to handle 10..99
    if (num >= 10 && num <= 19) {
        print10to19(num);
    }
    if (num >= 20 && num <= 99) {
        print20to99(num);
    } 
    if (num > 99 || num < 0) {
		cout << num << " is not in range from 1 to 99" << endl;	
	}
    return 0;
}

void print1to9(int n) {
// the function is not 100% correct, you'll need to debug!
// hint: what's the output if user input is 4 ?
    switch (n) {
        case 1:
            cout << "One";
            break;
        case 2:
            cout << "Two";
            break;
        case 3:
            cout << "Three";
            break;
        case 4:
            cout << "Four";
            break;
        case 5:
            cout << "Five";
            break;
            // add your logic here to handle 6..9
    	case 6:
	        cout << "Six";
	        break;
        case 7:
            cout << "Seven";
            break;
        case 8:
            cout << "Eight";
            break;
            // add your logic here to handle 6..9
    	case 9:
	        cout << "Nine";
	        break;
	        
	}
}

void print10to19(int n) {
    // complete the function print10to19
    switch (n) {
    	case 10:
            cout << "Ten";
            break;
        case 11:
            cout << "Eleven";
            break;
        case 12:
            cout << "Twelve";
            break;
        case 13:
            cout << "Thirteen";
            break;
        case 14:
            cout << "Fourteen";
            break;
        case 15:
            cout << "Fifteen";
            break;
            // add your logic here to handle 6..9
    	case 16:
	        cout << "Sixteen";
	        break;
        case 17:
            cout << "Seventeen";
            break;
        case 18:
            cout << "Eighteen";
            break;
            // add your logic here to handle 6..9
    	case 19:
	        cout << "Nineteen";
	        break;
	        
	}
}

void print20to99(int n) {
    // complete the function print20to99
    int shi = n / 10;
    switch (shi) {
        case 2:
            cout << "Twenty";
            break;
        case 3:
            cout << "Thirty";
            break;
        case 4:
            cout << "Forty";
            break;
        case 5:
            cout << "Fifty";
            break;
            // add your logic here to handle 6..9
    	case 6:
	        cout << "Sixty";
	        break;
        case 7:
            cout << "Seventy";
            break;
        case 8:
            cout << "Eighty";
            break;
            // add your logic here to handle 6..9
    	case 9:
	        cout << "Ninety";
	        break;  
	}
	int ge = n % 10;
	if (ge != 0) {
		cout << " ";
		print1to9(ge);  
	}
}
