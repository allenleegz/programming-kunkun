#include <iostream>

using namespace std;
const int N=10;

//TODO 1: Complete the function declaration, which
//        takes 2 integers using pass-by-reference
void Swap(int& a, int& b) { 
	//TODO2: Complete the function of Swap
	//       you may declare local variables if needed
	a = a + b;
	b = a - b;
	a = a - b;
}


int main() {
	int Num[10];
	int i,j;
	//cout << "Enter 10 numbers:\n";
	for (int i = 0; i < 10; i++) {
        cin >> Num[i];
	}
	//TODO3: Fill in the indexes of the 2 for-loop so
	//       as to sort numbers in ascending order
	
  	        		
//	1 4 5 2 6 6 8 3 9 10
	
	for (i = 0; i < 9; i++) {
		for(j = 0; j < 9 - i; j++) {
			if (Num[j+1] < Num[j]) {  //TODO4: Fill in the indices
				Swap(Num[j],Num[j+1]);
			}
		}
	}

	cout<<"The sorted list:\n";

	for (i=0;i<10;i++) {
		cout<<Num[i]<<" ";
	}
	cout<<endl;

}
