#include <iostream>
#include <iomanip>
#include <cstring>

using namespace std;

// 1. 找到下一个位置
// int& 什么意思？ 为什么这个参数多了一个& 表示如果我们在函数里面修改了这个参数的值，那么这个参数的值会真的被改变了。
// row  col  dir 三个小学生来课室上课， row  col 带了脑子（&）来，dir 只有人来了，脑子没来。上完课之后，前面两个小学生上课讲的知识就听进去了，最后一个就什么变化都没有。 
// 假设传的是分数，row dir原来的值为60， row 80，dir 60
// row  col 表示虫子当前的位置，dir表示虫子移动的方向
// 1 right 2 down 3 left 4 up 
void next(int& row, int& col, int dir) {
	switch (dir) {
		case 1:
			col++;
			break;
		case 2:
			row++;
			break;
		case 3: 
			col--;
			break;
		case 4:
			row--;
			break;
	}
}

// 回到上一个位置
// 完全就是上面的逆操作 
void pre(int& row, int& col, int dir) {
	switch (dir) {
		case 1:
			col--;
			break;
		case 2:
			row--;
			break;
		case 3:
			col++;
			break;
		case 4:
			row++;
			break;
	}
}

// 2. 判断位置是否越界（墙外）
// true 不在界外 false 在界外
// 
bool judge(int row, int col, int left, int right, int top, int bot) {
	if (col > right || col < left) return false;
	if (row > bot || row < top) return false;
	return true;
}

// 转变方向 
// 1. 修改边界的位置（墙的位置） 
// 2. 转变方向 
void changeDir(int& dir, int& left, int& right, int& top, int& bot) {
	switch (dir) {
		case 1:
			// 墙向下移 
			top++;
			break;
		case 2:
			// 墙向左移
			right--;
			break;
		case 3:
			// 墙向上移
			bot--;
			break;
		case 4:
			// 墙向右移
			left++;
			break;
	}
	/*
		1 2
		2 3
		3 4
		4 1 
	*/ 
	dir++;
	if (dir == 5) dir = 1;
}

// 初始化seq数组  
// seq是需要被修改的，但是不用加&，原因是数组都不需要加&，默认就是会。 
void getSeq(char seq[], char M[][70], int W, int H) {
	int index = 0;
	for(int k = 0; k < W; k++){
		for(int i = 0; i < H; i++){
			if (M[i][k] != '\0') {
				seq[index++] = M[i][k];		
			}
		}
	}	
}

// 找到某个字符在字符数组中的位置。
// 如果找到了就返回位置，找不到就返回 -1，表示不在这个字符数组中。 
// 题目说了如果有不在这70个字符内的字符就直接返回。 
int getIndex(char c, const char C[]) {
	for(int i = 0; i < 70; i++){
		if (C[i] == c) {
			return i;
		}
	}
	return -1;
}


class Codec {
private:
    int W, H, S;
    // const 常量 那么这个常量（对象）不能被修改的。C[0] = 'c' 不行会报错。 
    // 70个字母为什么要用71个格子，因为C字符串的末尾还有一个'\0'字符。 
	const char C[71] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890 .,-!?()";
    char M[70][70];
    
public:
	// 名字跟类名一样，无参构造函数constructor
	// 我们在创建Codec类的时候会调用的函数
	// Codec C; 
    Codec() {
    	
    	// 先把矩阵内所有的元素都清空，都设置为'\0' 
    	// clear
    	for(int i = 0; i < 70; i++){
			for(int k = 0; k < 70; k++){
				M[i][k] = '\0';
			}
		}

    	W = 70;
    	H = 1;
    	S = 0;
    	for(int i = 0; i < 70; i++){
			M[0][i] = C[i];
		}
    }
    // 有参构造函数 parameterized constructor 
	Codec(int w, int h, int s) {
		config(w, h, s);
    }
    void config(int w, int h, int s) {
    	W = w;
    	H = h;
    	S = s;
    	
    	// clear
    	for(int i = 0; i < 70; i++){
			for(int k = 0; k < 70; k++){
				M[i][k] = '\0';
			}
		}
    	
    	// 做这类题目的关键
		// 要有一个变量用来表示方向 
    	// 1 right 2 down 3 left 4 up
    	int dir = 1;
    	
    	// left、right、top、bot用来标识边界 
    	int left = 0, right = w - 1;
    	int top = 0, bot = h - 1;
    	
    	// 用来表示当前的位置（当前虫子的位置） 
    	int row = 0, col = 0;
    	
    	// 这层循环表示我们目前在放置第几个字母？
		// 总共有70个，for循环，i从0到69 
    	for(int i = 0; i < 70; i++){
    		// 需要移动几个格子？也就是说需要移动虫子times次
			// 第一个字母需要移动s次，否则移动s+1次
			// i 是否 == 0 ？ 如果是的话 times = s; 否则 times = s+1;
			/*
				int times;
				if (i == 0) {
    				times = s;
				} else {
					times = s+1;
				}
			*/ 
			int times = i == 0 ? s : s+1;
    		// 这层循环表示我们对虫子进行times次的移动。 
			for(int k = 0; k < times; k++){
				// 每次移动的操作：
				// 1. 找到下一个位置
				// 2. 判断位置是否越界（墙外）
				// 3. 如果没有就OK
				// 4. 如果有的话，回到上一个位置，转变方向，再走到下一个位置 
				
				// 1. 找到下一个位置
				next(row, col, dir);
				
				// 2. 判断位置是否越界（墙外）
				if (!judge(row, col, left, right, top, bot)) {
					
					// 回到上一个位置 
					pre(row, col, dir);
					
					// 转变方向 
					changeDir(dir, left, right, top, bot);
					
					// 再走到下一个位置 
					next(row, col, dir);
				}
			}
			
			// 移动到最终的位置，赋值 
			M[row][col] = C[i];
		}    	
    }
    void showSetting() {
    	
		// 打印参数 
    	cout << "W=" << W << ", H=" << H << ", S=" << S << endl;
    	
		// 打印矩阵 
		for(int i = 0; i < H; i++){
			for(int k = 0; k < W; k++){
				// 清空矩阵，把每一个格子都设置为'\0', 目的是区分开空格和初始化（空） 这两种情况 
				// 如果格子里面是 '\0' 相当于里面是空的，所以输出空格
				// 否则就直接打印。 
				if (M[i][k] == '\0') cout << ' ';
				else cout << M[i][k];
			}
			cout << endl;
		}
		
		// 打印密码工具串
		// 获取这个seq数组（字符串） 
		char seq[70];
		getSeq(seq, M, W, H);
		// 打印seq 
		cout << "Seq: ";
		for(int i = 0; i < 70; i++){
			cout << seq[i];
		}
		cout << endl;
		
    }
    // 加密 原文 in 密文 out 
    void encode(char in[], char out[]) {
    	
		// 1. 获取加密的工具串，也就是seq数组 
    	char seq[70];
    	getSeq(seq, M, W, H);
    	
    	// 2. 对in数组里面的每一个字符，都找到它对应的密文，塞到out里面
    	// 找到它对应的密文：找到这个字符的位置，去另一个串里面找对应位置的字符。 
    	int i = 0;
		for( ; ; i++){
			if (in[i] == '\0') break;
			int index = getIndex(in[i], C);
			// 不在字符数组中，也就是说不用转化 
			if (index == -1) out[i] = in[i];
			// 在字符数组中，就把密文串对应的位置的字符塞到out 
			else out[i] = seq[index];
		}
		// 字符串最后都要有一个'\0' 
		out[i] = '\0';
    }
    // 解密 密文 in 原文 out 
    void decode(char in[], char out[]) {
    	// 1. 获取加密的工具串，也就是seq数组
    	char seq[70];
		getSeq(seq, M, W, H);
		
		
		int i = 0;
		for(;; i++){
			if (in[i] == '\0') break;
			int index = getIndex(in[i], seq);
			if (index == -1) out[i] = in[i];
			// 区别点，找到原文串对应的位置的字符塞到out 
			else out[i] = C[index];
		}
		out[i] = '\0';
    }
};

int main() {
    char cmd, inStr[1000], outStr[1000];
    bool fin=false;
    // 创建一个Codec类，名为 C，此时会调用无参构造函数。 
    Codec C;
    
    while(!fin) {
        cout << "\nCommand: ";
        cmd = cin.get();
        switch(cmd) {
            case 'C': 
                int w,h,s;
                cin>>w>>h>>s;
                C.config(w,h,s);
                cin.ignore(1000,'\n');
                break;
            case 'Q':
                cout << "Quit\n";
                cin.ignore(1000,'\n');
                fin=true;
                break;
            case 'S':
                C.showSetting();
                cin.ignore(1000,'\n');
                break;
            case 'E':
                cin.get();
                cin.getline(inStr,1000);
                C.encode(inStr,outStr);
                cout << outStr<<endl;
                break;
            case 'D':
                cin.get();
                cin.getline(inStr,1000);
                C.decode(inStr,outStr);
                cout << outStr<<endl;
                break;
        }
    }
    return 0;
}
