#include<iostream>
#include<iomanip>
using namespace std;

int main() {		
	cout << "Enter the number of rows: ";
	int num;
	cin >> num;
	
	if (num <= 0) {
		cout << "Please enter positive integer." << endl;
	}
	
	for (int row = 0; row < num; row++){
		
		for (int i = 0; i < row; i++) {
			cout << row - i << " ";
		}
		
		cout << "0 ";
		
		int i = 1;
		for (; i < num - 1 - row; i++) {
			cout << i << " ";
		}
		if (row != num - 1) {
			cout << i;		
		} 
		
		cout << endl;
	}
	return 0;
}
