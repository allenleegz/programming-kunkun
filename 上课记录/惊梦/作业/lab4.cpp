#include<iostream>
#include<iomanip>
using namespace std;

int main() {		
	int a, b;
	char op;
	string s;
	cout << "Enter the equation : ";

	cin >> a >> op >> b;
	if (!cin.good()) {
		cout << "Invalid input." << endl;
		return 0;
	}

	switch (op) {
		case '+':
			printf("%d+%d=%d\n", a, b, a+b); 
			break;
		case '-':
			printf("%d-%d=%d\n", a, b, a-b);
			break;
		case '*':
			printf("%d*%d=%d\n", a, b, a*b); 
			break;
		case '/':
			cout << a << "/" << b << "=" << 
			setprecision(6) << (double)a/b << endl;
			break;
		case '<': 
			s = a < b ? "T" : "F"; 
			printf("%d<%d=%s\n", a, b, s.c_str()); 
			break;
		case '>':
			s = a > b ? "T" : "F";
			printf("%d>%d=%s\n", a, b, s.c_str());
			break;		
		case '=':
			s = a == b ? "T" : "F";
			printf("(%d==%d)=%s\n", a, b, s.c_str());
			break;
		default:
			cout << "Invalid operation.\n";
			break;
	}
	return 0;
}
