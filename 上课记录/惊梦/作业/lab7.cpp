#include<iostream>
using namespace std;

// inline 内联 是一种速度优化的方法
// ? : 三目运算符
// 逻辑运算式（true 或者 false） ? true的时候取的值 : false的时候取的值
// int a = 3;
// int b = a == 3 ? 4 : 5; // 所以 b 等于多少? b = 4
// 记忆方法 a == 3 吗？ 如果等于就是 4 ，否则就是 5
inline int gcd(int a, int b) {
	return b > 0 ? gcd(b, a % b) : a;
}

/**
 *	第一种解法：
 *      32 6 的 GCD ： 6 5 4 3 2 从较小值开始遍历
 */

// 辗转相除法
int gcd_1(int a, int b) {
	// a = 45 b = 9  -->  被整除

	// 9 ÷45 = 0 ...... 9
	// a = 9  b = 45  -->  a = 45 b = 9  -->  gcd = 9
	// a = 36 b = 24  -->  a = 24 b = 12  -->  gcd = 12

	// 我们要做的是 把 b 赋值给 a，把 a%b 赋值给 b
//	temp = a;
//	a = b;
//	b = temp % b;
	//  	     a        b      temp  
	//        【可乐】 【雪碧】 【】
	//  第一步【可乐】 【雪碧】 【雪碧】 temp = b; 
	//  第二步【可乐】 【可乐】 【雪碧】 b = a;
	//  第三步【雪碧】 【可乐】 【雪碧】 a = temp;		 
	int temp; 
	
	/*
	for(XX; XX; XX) 
	for(int i = 0; i < 10; i++) 
	for (;a % b != 0;) {
		temp = b; // 24
		b = a % b; // 12
		a = temp; // 24
	}
	
	*/ 
	while (a % b != 0) {
		temp = b; // 24
		b = a % b; // 12
		a = temp; // 24
	}

	return b;
}

int main1() {
	cout << 9 % 45;
	return 0;
}

int main() {
	int nums[5];
	cout << "Enter 5 numbers:" << endl;
	for (int i = 0; i < 5; i++) {
		cin >> nums[i];
	}
	cout << "GCD:" << endl;
	int temp = nums[0];
	for (int i = 1; i <= 4; i++) {
		// return后等于 temp = 9;
		temp = gcd(temp, nums[i]);
	}
	cout << temp;

	return 0;
}
