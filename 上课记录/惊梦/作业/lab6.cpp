#include<iostream>
using namespace std;

int main() {
	int num1[10];
	int num2[10];
	int num3[10];
	cout << "Enter 10 Elements of Set A:" << endl;
	for(int i=0;i<10;i++){
		cin >> num1[i];
	}
	cout << "Enter 10 Elements of Set B:" << endl;
	for(int i=0;i<10;i++){
		cin >> num2[i];
	}
	int k = 0;
	// num1   [3 5 1 8]
	// num2   [8 2 4 3]
	for(int i=0;i<10;i++){
		for(int j=0;j<10;j++){
			if (num1[i] == num2[j]) {
				num3[k] = num1[i];
				k++;
				break;
			}
		}
	}
	
	// num3  [3 5 1 8] , k = 4
	if (k == 0) {
		cout << "The Intersected Element of Set A and\nB are not Found." << endl;
	} else {
		cout << "The Intersected Elements of Set A\nand B are:" << endl;
		for(int i=0;i<k-1;i++){
			cout << num3[i] << " ";
		}
		cout << num3[k-1] << endl;
	}
	return 0;
} 
