#include <iostream>
#include <iomanip>

using namespace std;

int main() {

	double a,b;
	char o;
	cout << "Enter the equation : \n";
	cin >> a >> o >> b;
	
	if (!cin.fail()) {
		if (o='=') {
			cout << '(' << a << "==" << b << ')' << '=' << ((a==b)?'T':'F') << endl;
		} else {
			
			cout << a << o << b << '=';
		
			switch (o) {
				case '<':
					cout << ((a<b)?'T':'F') << endl;
					break;
		
				case '>':
					cout << ((a>b)?'T':'F') << endl;
					break;
		
				case '+':
					cout << a + b << endl;
					break;
		
				case '-':
					cout << a - b << endl;
					break;
		
				case '*':
					cout << a * b << endl;
					break;
		
				case '/':
					cout << a / b << endl;
		
				default :
					cout << "Invalid operation.\n";
			}
		}			
	}
	else cout << "Invalid input.\n";
}