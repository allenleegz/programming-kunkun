#include <iostream>
using namespace std;

class Set {
private:
    int nElement, Elements[5];

//To-do: Complete the implementation of the constructors and member functions    
public:
    Set() {
    	nElement = 0; 
	}
    Set(int X[]) {
    	for(int i = 0; i < 5; i++){
			Elements[i] = X[i];
		}
		nElement = 5;
    }
    void display() {
    	cout << "{";
    	for(int i = 0; i < nElement - 1; i++){
			cout << Elements[i] << ",";
		}
		if (nElement > 0) {
			cout << Elements[nElement-1];
		}
		cout << "}";
    }
    void insert(int e) {
    	for(int i = 0; i < nElement; i++){
			if (Elements[i] == e) {
				return;
			}
		}
		Elements[nElement++] = e;
    }
    // 重点看看 
    bool equals(Set S) {
    	if (S.getNElements() != nElement) return false;
    	for(int i = 0; i < S.getNElements(); i++){
    		// 表示是否存在相同的元素 
			bool flag = false;
			for(int j = 0; j < nElement; j++){
				if (S.getElements()[i] == Elements[j]) {
					flag = true;
					break;
				}
			}
			if (flag == false) {
				return false;
			}
		}
		return true;
    }
    void copy(Set S) {
    	nElement = S.getNElements();
    	for(int i = 0; i < S.getNElements(); i++){
			Elements[i] = S.getElements()[i];
		}
    }
    int getNElements() {
		return nElement;
	}
	// 函数返回值是数组
	// int数组 int* 
	int* getElements() {
		return Elements; 
	}
};

//DO NOT modify the main() function
int main() {
    int data[5]={1,3,5,7,9};
    Set A, B(data), C;
    cout << "A contains: ";
    A.display();
    cout<<endl;
    
    cout << "B contains: ";
    B.display();
    cout<<endl;
    
    cout << "A is ";
    if (!A.equals(B)) cout <<"not ";
    cout << "the same as B\n";
    
   
    int i;
    cout << "Enter no more than 5 unique numbers and ends with -999: ";
    while(1) {
        cin >>i;
        if (i==-999) break;
        A.insert(i);
    }
    
    cout << "Enter no more than 5 unique numbers and ends with -999: ";
    while(1) {
        cin >>i;
        if (i==-999) break;
        C.insert(i);
    }
    
    cout << "A contains: ";
    A.display();
    cout<<endl;
    
    cout << "C contains: ";
    C.display();
    cout<<endl;

    cout << "A is ";
    if (!A.equals(B)) cout <<"not ";
    cout << "the same as B\n";
    
    cout << "A is ";
    if (!A.equals(C)) cout <<"not ";
    cout << "the same as C\n";
    
    cout << "Now copying C to A...\n";
    A.copy(C);
    cout << "A is ";
    if (!A.equals(C)) cout <<"not ";
    cout << "the same as C\n";
    
}
