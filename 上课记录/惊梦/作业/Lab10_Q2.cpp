#include<iostream>
#include<algorithm>
#include<iomanip>
#include<cmath>


using namespace std;

int main() {
	
//	char s[50];
//	cin.getline(s, 50); 
//	
//	for(int i = 0; i < 50; i++){
//		if (s[i] == '\0') {
//			break;
//		}
//		
//		if (s[i] >= 'A' && s[i] <= 'Z') {
//			s[i] += 32;
//		} else if (s[i] >= 'a' && s[i] <= 'z') {
//			s[i] -= 32;
//		}
//		
//	}
//	
//	for(int i = 0; i < 50; i++){
//		if (s[i] == '\0') {
//			break;
//		}
//		
//		cout << s[i];
//		
//	}


	// case 1
//	int a = 50;
//	
//	if (a == 50) {
//		a--;
//	} else if (a == 49) {
//		a--;
//	}
//	
//	cout << a; // 48 错的，应该是 49 
	
	/* 
		前面如果有满足条件的，后面的就不会去判断
		
		
		if(有西红柿) {
			买一个            // 如果菜市场西红柿、西瓜、鸡蛋都有，你也只会买一个西红柿就走了。		
		} else if(有西瓜) {
			买一个 			  // 如果菜市场西瓜、鸡蛋都有，你也只会买一个西红柿就走了。
		} else {
			买一个鸡蛋
		}
		
		if(有西红柿) {
			买一个 
		} else {
			买一个鸡蛋 
		}
		
		
		if(有西红柿) {
			买一个            // 如果菜市场西红柿、西瓜、鸡蛋都有，你会买一个西红柿跟一个西瓜。		
		} 
		if(有西瓜) {
			买一个 			  // 如果菜市场西瓜、鸡蛋都有，你也只会买一个西瓜。
		} else {
			买一个鸡蛋 
		} 		
	*/

	// case 2
//	int a = 50;
//	
//	if (a == 50) {
//		a--;
//	} 
//	
//	if (a == 49) {
//		a--;
//	}
//	
//	cout << a; // 48 
	

	char a[50];
//	cin >> a;
	cin.getline(a, 50);
	
	
	for(int i = 0; i < 50; i++){
		if (a[i] == '\0') {
			break;
		}
		
		if (a[i] >= 'A' && a[i] <= 'Z') {
			a[i] += 'a'-'A';
		} else if (a[i] >= 'a' && a[i] <= 'z') {
			a[i] -= 'a'-'A';
		} 

//      为什么不可以这么写？
//      在上面已经把该字母转成了小写字母了。		
//		if (a[i] >= 'a' && a[i] <= 'z') {
//			a[i] -= 'a'-'A';
//		}
		
		
	}



		











	
	return 0;
}
