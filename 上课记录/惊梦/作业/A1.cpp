#include<iostream>
#include<iomanip>

using namespace std;

const char* MONTHS[] = {"JAN", "FEB","MAR", "APR", "MAY", "JUN", 
						"JUL", "AUG", "SEP", "OCT", "NOV", "DEC"};

const int MONTHS_DAYS[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

char ss[6];

bool isLeapYear(int i1) {
	if (i1 % 4 == 0 && i1 % 100 != 0) {
		return true;
	} 
	if (i1 % 400 == 0) {
		return true;
	}
	return false;
} 

/**
 * @brief 0 means error; 1 means only one option; 2 means two options.
 * 
 * @param i1 
 * @param i3 
 * 
 * @return 
 **/
int judgeTimeFormat(int& i1, int& i3) {
	// if i1 is year
	int temp;
	if (i1 == 0 || i1 > 31) {
		return (i3 >= 1 && i3 <= 31);
	}
	// if i3 is year
	if (i3 == 0 || i3 > 31) {
		temp = i3; i3 = i1; i1 = temp;	
		return (i3 >= 1 && i3 <= 31);
	}
	// i1 and i3 may be year or day
	return 2; 
}

/**
 * @brief only deal with single option
 * 
 * @param i1 
 * @param month 
 * @param i3 
 * 
 * @return 
 **/
bool validateTime(int year, int month, int day) {
	// validate months
	if (month <= 0 || month > 12) {
		return false;
	}
	// year, month and day are fixed
	if (month == 4 || month == 6 || month == 9 || month == 11) {
		return day <= 30;
	}
	if (month == 2) {
		if (isLeapYear(year)) {
			return day <= 29;	
		} else {
			return day <= 28;
		}			
	}
	return true;
}

int getDays(int year, int month, int day) {
	int days = day - 1;
	for(int i = 1950; i < year; i++){
		if(isLeapYear(i)){
			days += 366;
		} else {
			days += 365;
		}
	}
	int diffMonth = month - 1;
	bool isLeap = isLeapYear(year);
	for (int i = 0; i < diffMonth; i++) {
		if (i == 1 && isLeap) {
			days += 1;
		} 
		days += MONTHS_DAYS[i];
	}
	return days; 
}

int getDiff(int year1, int month1, int day1, int year2, int month2, int day2) {
	int days1 = getDays(year1, month1, day1);
	int days2 = getDays(year2, month2, day2);
	
	return days2 - days1; 
}

void printAnswer(int year, int month, int day, char s[]) {
	cout << s;
	printf(" should be %02d-%s-%d\n", day, MONTHS[month-1], year);
}

int handleYear(int i1) {
	if (i1 < 50) {
		return i1 + 2000;
	} else {
		return i1 + 1900;
	}
}

int calLen(int num) {
	int cnt = 0;
	while(num != 0) {
		cnt++;
		num /= 10;
	}
	return cnt;
}

void printDoubleAnswer(int i1, int month, int i3, char s[], int diff) {
	cout << s;
	printf(" could be %02d-%s-%d or %02d-%s-%d\n", i3, MONTHS[month-1], handleYear(i1),
	 	i1, MONTHS[month-1], handleYear(i3));
	int strLen = calLen(diff);
	for(int i = 0; i < 52 + strLen; i++){
		cout << "#";
	}
	cout << endl; 
	printf("# The ambiguity causes a time difference of %d days! #\n", diff); 
	for(int i = 0; i < 52 + strLen; i++){
		cout << "#";	
	}
	cout << endl;	
}


int A1() {
	
	// handle input and output
	cout << "Input the six-digit date: ";
	cin >> ss;
	
	int s = atoi(ss);
	
	// handle input
	int i1 = s / 10000, month = (s / 100) % 100, i3 = s % 100;
		
	// validate input time
	int option = judgeTimeFormat(i1, i3);
	if (!option) {
		cout << ss << " is not a valid date." << endl;
		return 0;
	}
	// single option
	if (option == 1) {
		if (!validateTime(handleYear(i1), month, i3)) {
			cout << ss << " is not a valid date." << endl;
		} else {
			printAnswer(handleYear(i1), month, i3, ss);
		}
	}
	// double options
	else {
		// first option is wrong
		if (!validateTime(handleYear(i1), month, i3)) {
			// two options are wrong
			if (!validateTime(handleYear(i3), month, i1)) {
				cout << ss << " is not a valid date." << endl;
			}
			// only second option is right
			else {
				printAnswer(handleYear(i3), month, i1, ss);
			}
		} else {
			// two options are right 
			if (validateTime(handleYear(i3), month, i1)) {
				int diff = getDiff(handleYear(i1), month, i3, handleYear(i3), month, i1);
				if (diff < 0) {
					printDoubleAnswer(i3, month, i1, ss, -diff);
				} else if (diff == 0) {
					printAnswer(handleYear(i1), month, i3, ss);	
				} else {
					printDoubleAnswer(i1, month, i3, ss, diff);	
				}			 
			}
			// only first option is right
			else {
				printAnswer(handleYear(i1), month, i3, ss);
			}	
		}
	}	
	return 0;
}
int main() {
//	A1();

	// Self-Test
	while(1){
		A1();
	}
}
