#include<iostream>
using namespace std;
 
const int HOURS_IN_DAY = 24;
const int MINUTES_IN_HOUR = 60;
const int SECONDS_IN_MINUTE = 60;
 
long long getTimeStamp(int day, int hour, int minute, int second) {
	return ((day * HOURS_IN_DAY + hour) * MINUTES_IN_HOUR + minute) * SECONDS_IN_MINUTE + second;
} 

void printResult(long long timeStamp) {
	int seconds = timeStamp % SECONDS_IN_MINUTE;
	int minutes = ((timeStamp - seconds) / SECONDS_IN_MINUTE) % MINUTES_IN_HOUR;
	int hours = (timeStamp / (MINUTES_IN_HOUR * SECONDS_IN_MINUTE)) % HOURS_IN_DAY;
	int days = timeStamp / (HOURS_IN_DAY * MINUTES_IN_HOUR * SECONDS_IN_MINUTE);
	
	printf("The time difference is: %d day(s), %d hour(s), %d minute(s), and %d second(s).\n",
		days, hours, minutes, seconds);
} 

int main() {
	int startDay, startHour, startMinute, startSecond;
	int endDay, endHour, endMinute, endSecond;
	
	cout << "For start time, please enter the following information:" << endl;
	cout << "Enter the start day (a number in range from 1 to 31):";
	cin >> startDay;
	cout << "Enter the start hour (a number in range from 0 to 23):";
	cin >> startHour;
	cout << "Enter the start minute (a number in range from 0 to 59):";
	cin >> startMinute;
	cout << "Enter the start second (a number in range from 0 to 59):";
	cin >> startSecond;	
	
	cout << "For end time, please also enter the similar information:" << endl;
	cout << "Enter the end day (a number in range from 1 to 31):";
	cin >> endDay;
	cout << "Enter the end hour (a number in range from 0 to 23):";
	cin >> endHour;
	cout << "Enter the end minute (a number in range from 0 to 59):";
	cin >> endMinute;
	cout << "Enter the end second (a number in range from 0 to 59):";
	cin >> endSecond;	
	
	
	long long timeDiff = getTimeStamp(endDay, endHour, endMinute, endSecond) -
		getTimeStamp(startDay, startHour, startMinute, startSecond);	
	
	printResult(timeDiff);	
	
	return 0;
}
