#include<iostream>
#include<algorithm>
#include<iomanip>
 
using namespace std;


void q1() {
	int num[10]={5,10,2,5,8,8,7,9,1,5};
	
	for(int i = 0; i < 10; i++){
		for(int j=0; j<num[i]; j++){
			cout << "*"; 
		}
		cout << endl;
	}	
}

void q2() {
	int num[10]={5,10,2,5,8,8,7,9,1,5};
	// 升序排序 
	sort(num, num+10); 
	// 如果需要降序
//	reverse(num, num+10);
	
	for(int i = 0; i < 10; i++){
		for(int j=0; j<num[i]; j++){
			cout << "*"; 
		}
		cout << endl;
	}
}

void q3() {
	cout << "Number of students?" << endl;
	int n, sum = 0;
	cin >> n;
	
	int num[20];
	for(int i = 0; i < n; i++) { 
		cout << "Student " << i+1 << ":" << endl;
		cin >> num[i];
		sum += num[i];
	}
	/*
		a 0 1 2 3 
		b 1 3 5 7
		
		b = 2 * a + 1
	*/
	
	// << fixed << setprecision(2) 保留两位小数
	cout << "Average = " << fixed << setprecision(2) << (double)sum / n << endl;
	
	for(int i = 0; i < n; i++){
		for(int j=0; j<num[i]; j++){
			cout << "*"; 
		}
		cout << endl;
	}
}

int main() {
	q3();

//	while(1){
//		q1();
//	} 

	return 0;
}
