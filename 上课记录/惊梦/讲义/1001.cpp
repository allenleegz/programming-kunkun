#include <iostream>
#include <iomanip>
#include <bits/stdc++.h> 
using namespace std;

int main() {
	/*
	
	if (<condition>) <statement>;
	<> 可能有的
	
	// 1，推荐 
	if () {
		执行语句;
	} 
	
	// 2
	if () 执行语句;  
	
	里面的条件判断语句
	1. x < 0 
	2. x = 0 错误 x == 0
	
	if () {
		XXX;	
	} else {
		YYY;
	}
	
	best practice:
	// 否定先行
	// else 可以省略的情况
	 
	int main() {
		int score;
		cin >> score; 
		if (score >= 60) {
			cout << "pass";
		} else {
			cout << "fail";
		}	
	}
	
	int x=5;
	if (x==5) {;
		x=3;
	} 
	cout << x; 3 
	
	int main() {
		int score;
		cin >> score; 
		if (score < 0 || score > 100) {
			cout << "error";
			return;
		}
		if (score >= 60) {
			cout << "pass";
			return; 
		}
		cout << "fail";	
	}
	
	int x;
	if (x == 6) {
		cout << "上课"; 
	} else if (x == 7) {
		cout << "写作业";
	} else if (x == 1) {
		cout << "谭仔米线";
	} else {
		cout << "学校的课";
	} 
	 
	// 整一个 if 的结构被看作是一个语句 
	if (a==1) 
		if (b==2) 
			cout << "***\n";	
	else
		cout << "###\n";
	
	条件判断语句，除了0以外都认为是true 
	
	!！()（）
	!= 不等于
	! 取反
	
	逻辑运算符
	&&  ||  ! 
	 
	
	// 函数执行完毕，把结果返回给别人。 
	// else 会跟离他最近的那个 if 关联起来。
	 
//	int a = 1, b = 3; 
//	if (1)
//		if (b==2) 
//			cout << "***\n";		
//		else
//			cout << "###\n";

//	int x = 3;
//	if (!(x > 3)) {
//		cout << "HHH";
//	} else {
//		cout << "KKK";
//	}
	
	// | & ^ 位运算  
//	3       +  8 
//	  0011	
// +  1000
// -------- 
// =  1011

// |  1|1=1  0|1=1  1|0=1  0|0=0
// &  1&1=1  0&1=0  1&0=0  0&0=0
// ^ 异或 也可以理解为二进制的加法 也可以理解为两个数不同则为1，相同则为0 
// ^  1^1=0  0^1=1  1^0=1  0^0=0  
	
//	  0011	
// &  1000
// -------- 
// =  0000

//	  0011	
// |  1000
// -------- 
// =  1011 8+2+1 = 11

//	  0011	
// ^  1000
// -------- 
// =  1011 8+2+1 = 11

// 1 2 4 8 16 32 64 128 256 512 1024
//            5                 10
// USB 1GB =  1024 MB = 1024 * 1024 = ...KB  1000
//     1GB -> 1000 MB  
	
//	int a = 3;
//	int b = 8;
//	int c = a & b; // 
//	c = a | b; // 11
//	c = a ^ b; // 11
//	cout << c;
	
	!(a && b) <--> !a || !b
	!(a || b) <--> !a && !b
	 
	
	*/
	
//  and 用法 
//	int x = 3, y = 5;
//	if (x == 3 and y == 5) {
//		cout << "HHH";
//	} 

// 字符比较 ， ASCII 字符转成数字 
//	if ('A' < 'Z') {
//	} 

	// 判断一个字符是否为小写字母？
	// 小写字母比大写字母的 ASCII 数字要大 
//	char c;
//	cin >> c;
//	if (c >= 'A' && c <= 'Z') {
//		cout << c;	
//	} else if (c >= 'a' && c <= 'z') {
//		cout << (char) (c - ('b' - 'B'));
//	}
//	
	// 先行判断的意思
	// && || 短路运算符
//	A || B && C 
	
	// 常用 
//	if ( b != 0 && a / b > 5)
	
//	int a = 2;
//	switch (a) {
//		case 1:
//		case 2:	
//			cout << "1122";
//			break;
//		case 5:
//			cout << "55";
//			break;	
//		default:
//			cout << "default";
//			break;
//	} 

	// 双目运算符
	// ? :
//	int a = 35, b = 67;
//	int min = a > b ? b : a;  
//	cout << min; 
	
	 
//	int a, b;
//	cin >> a >> b;
//	cout << a << endl << b << endl; 
	int a, b;
	char op;
	cout << "Enter the equation: ";
	cin >> a >> op >> b;
	// Verify whether the input ‘a’ and ‘b’ are digits.
	if (!isdigit(a) || !isdigit(b)) {
		cout << "Invalid input." << endl;
	}
	
	return 0; 
}
