#include<iostream>
#include<algorithm>
#include<iomanip>
 
using namespace std;

/*
	返回值的类型(没有返回值用void)		
	函数声明 ： 如果函数写在 main 函数的后面，则需要声明。
	
	三种调用函数的方式：传值调用、指针调用、引用调用 
	1. 传值调用：在函数内修改，不影响原来的值，就是最简单的调用函数的方式。 
	   void printHello (int n)
	2. 引用调用：Pass-by-Reference 
	
	参数可以设置默认值，	 
*/

//void printHello (int n); 

void printHello (int n = 10) {
	for(int i=0;i<n;i++){
		cout << "HELLO" << endl;
	} 
}

void printHello1 () {
	for(int i=0;i<8;i++){
		cout << "HELLO" << endl;
	} 
}

void f(int x[]) {
	x[0] = 30; 
} 

int factorial(int n)
{
	if (n==1) {
		return 1; 
	}
	return n * factorial(n-1);
}
// 主函数 
int main() {
//	int a = 1; 
//	printHello(a); 
//	cout << a;
	
//	printHello1();

	// 参数传的是一个数组，此时默认就是引用传递。 
//	int a[3]={1,2,5};
//	f(a); 
//	cout << a[0];

	// Recursion 递归 递归的函数：会调用自身 
	// Iterative 迭代 循环的方法解决 
	 
	return 0;
}

