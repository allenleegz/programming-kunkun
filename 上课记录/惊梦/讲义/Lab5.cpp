#include<iostream>
using namespace std;

// 找因数 
void q1() {
	cout << "Enter a Number in Range [2 to N]:" << endl;
	int a, count = 0, sum = 0;
	cin >> a;
	
	if (a < 0) {
		cout << "Error! Input can't be a negative number." << endl;
		return ;
	} else if (a == 0) {
		cout << "Error! Input can't be zero." << endl;
		return ;
	} else if (a == 1) {
		cout << "Error! Input can't be one." << endl;
		return ;
	}
	
	// 1. 表示有没有因数 2. 表示有没有输出 "The Factor(s) of "
	bool flag = false;  
	for(int i = 2; i < a; i++){
		if (a % i == 0) {
			// Total Factors 
			count++;
			// Sum of Factors   sum += i; -> sum = sum + i; 
			sum += i; 
			if (flag == false) {
				cout << "The Factor(s) of " << a << " are:";
				flag = true;
			}
			cout << " " << i;
		}
	}
	
	if (flag == false) {
		cout << "No Factor for the Number " << a;
	}	
	cout << endl;
	
	cout << "Total Factors are: " << count << endl;
	cout << "Sum of Factors is: " << sum << endl;		
}

void q2() {
	int positiveNum = 0, negativeNum = 0, zeroNum = 0, positiveSum = 0;
	int negativeSum = 0;
	
	cout << "Enter Numbers! Enter -999 to Stop:" << endl;
	
	int a;
	
	// 1 true while(1) 一直循环直到被 break 
	while(1) {
		cin >> a;
		if(a == -999){
			break;
		}
		
		if (a > 0) {
			positiveNum ++;
			positiveSum += a;
		} else if (a == 0) {
			zeroNum ++;
		} else {
			negativeNum ++;
			negativeSum += a;
		}
	}
	
	cout << "Total Positive Numbers are: " << positiveNum << endl;
	cout << "Total Negative Numbers are: " << negativeNum << endl; 
	cout << "Total Zeros are: " << zeroNum << endl;
	cout << "Sum of Positive Numbers is: " << positiveSum << endl;
	cout << "Sum of Negative Numbers is: " << negativeSum << endl;
	
	if (positiveNum) {
		cout << "Average of Positive Numbers is: " << (double)positiveSum / positiveNum << endl;	
	} else {
		cout << "Average of Positive Numbers is: 0" << endl;
	}
	 
}

int main() {

	while(1){
		q2();
	} 

	return 0;
}
