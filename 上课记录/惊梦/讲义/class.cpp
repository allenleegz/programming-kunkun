#include<iostream>
#include<algorithm>
#include<iomanip>
 
using namespace std;

/*
	C语言 面向过程的语言 把大象放进冰箱：
		1. 把冰箱门打开；
		2. 把大象塞进去；
		3. 把老虎塞进去；
		4. 把老虎拿出来；
		5. 把大象拿出来；
		6. 把老虎塞进去； 
		7. 把冰箱门关闭。
		 
	Object-oriented 面向对象，一个人学会了喝奶茶，那他还需要学会喝老火汤？那他还需要学会喝粥？
	一个人学会了搬行李，那他还需要学会搬矿泉水？ 
	一个对象有某种能力，我们让这个对象去做某件事情。
	C++  面向对象的语言
		1. new 冰箱, 大象, 老虎; 
		2. 冰箱.塞(大象)
		2. 冰箱.塞(老虎)
	
	Human human = new Human();
	Dog dog = new Dog();
	dog.bark();
	dog.run();
	while(true) {
		dog.run();
		if (dog.see(human)) {
			dog.bark();
		}
	}
				 
*/ 

/**
 * public 类的外部是可访问的
 */


class/*关键字*/ Dog/*类的名称*/ {
	public:/*访问修饰符*/
		double weight;
		double speed;
		int lifeSpan;
		string name;	
		
		void bark() {
			cout << name << ": WoW Wow WoW!" << endl; 
		};
		
		// 无参构造方法
		Dog();
		
		Dog(string name);
		
		Dog(string name, double speed);
		
		Dog(string name, string name2);
};

Dog::Dog() {
    cout << "Creating a dog with no name " << endl;
    this->name = "default";
}

Dog::Dog(string name) {
    this->name = name;
}

// 两个参数的方法
Dog::Dog(string name, double speed) {
    this->name = name;
    this->speed = speed;
}

// 两个参数的方法，不过是两个 string
Dog::Dog(string name, string name2) {
    this->name = name + name2;
}

class Human {
  protected:
	   string name;	
};

class Programmer : public Human {
	public:
		string language;
    string getName() {return name;}
};
 
int main() {
	// 1. 怎么定义一个对象（类）？
	// Dog dog1;
	// Dog dog2;
	// dog1.name = "旺财";
	// dog2.name = "小强";
	// dog1.lifeSpan = 10;
	// dog2.lifeSpan = 20;
  // 
	// int year = 2022;
  // 
	// while(true) {
	// 	if (dog1.lifeSpan > 0) dog1.lifeSpan--;
	// 	if (dog2.lifeSpan > 0) dog2.lifeSpan--;
  // 
	// 	if (dog1.lifeSpan == 0) {
	// 		cout << dog1.name << " die at " << year << endl;
	// 		if (dog2.lifeSpan > 0) dog2.bark();
	// 		dog1.lifeSpan = -1;
	// 	}
	// 	if (dog2.lifeSpan == 0) {
	// 		cout << dog2.name << " die at " << year << endl;
	// 		if (dog1.lifeSpan > 0) dog1.bark();
	// 		dog2.lifeSpan = -1;
	// 	}
  // 
	// 	if (dog1.lifeSpan <= 0 && dog2.lifeSpan <= 0) break;
  // 
	// 	year++;
	// } 

  // 2. 访问修饰符
//   Human human;
  // human.name = "Allen"; // protected 不能被访问

//   Programmer pp;
//   cout << pp.getName();
//   pp.language = "C++";
//   Programmer copy = pp;
//   cout << copy.getName();


    // 3. 构造器
    // Dog dog("旺财");
    // // 两个参数的方法
    // Dog dog2("xiaobai", 10.0);
    // Dog dog3("AAA", "BBB");
    // cout << dog.name << endl;
    // cout << dog2.name << endl;
    // cout << dog3.name << endl;
    // 无参构造方法
    // Dog dogNoName; // Dog dogNoName()  error
    // cout << dogNoName.name << endl;
    
    
    // A friend function 不是类的成员函数，但是可以访问 private 不是类的成员函数
    // 需要在类的内部声明一下。
	return 0;
}
