#include<iostream>
#include<algorithm>
#include<iomanip>
//#include<string>
#include <cstring>
 
using namespace std;

int main() {
	/*
	字符 Character '' 1B
	字符串 String ""

	小写字符 - 对应的大写字符 是一个固定值
	小写字符的数字比较大

	char xxx;
	'c' - 32  =>  'c' - ('a' - 'A')
	小写转大写：xxx - ('a' - 'A')
	大写转小写： xxx + ('a' - 'A')

	判断字符类型：
	c>='0' && c<='9'
	c>=0 && c<=9

	cin.get() 不会跳过空格
	 
	两种类型的 string
		1. char 数组 cstring 在 C语言里面定义的内容 
		2. std::string 写在 C++ <string> 
		
	cstring:
		'\0' 表示第1个字符，也就是在ASCII码表上面为0的字符，NUL 
		1. char数组，数组的末尾是 '\0' 注意跟 '0' 的区别
		2. 字符串都有一个 '\0' 在末尾，n 个元素大小的数组最多只能存 n-1 个字符
		 
	声明 / 初始化：
	 	char name[10]; 
	 	char name[5] = "John"; 
	 	char name[] = "John"; 
	 	char name[5] = {'J','o','h','n','\0'};
	
	注意：不能在声明后初始化字符串
	char name[10]; 
	name = "john"; //  cannot convert from 'const char [5]' to 'char [10]'
	"john" 字符串，常量，字面量， 不能给变量赋值一个常量。 
	只需记得，先声明，后初始化，不行!
	
	
	cin 的问题 
		1. cin >> str 碰到空格，tab，回车，都会停止输入；
		2. 使用 cin >> 不会检查输入的长度 
	
	所以我们可以用这个 cin.getline，空格、Tab也可以读入，
	碰到回车才停止，或者读了指定数量的字符才停止。 
	
	
	为什么需要 '\0' 字符？
		1. 当我们传递字符串（数组）给函数的时候，实际上传的是字符串的开头地址；
		2. cout 这个函数在打印字符串的时候，只是去判断打印到 '\0' 就不打了。
		
	
	把字符串传递给函数，字符串是数组，数组传递给函数都是通过引用传递
	通过引用传递的方式，我们修改入参会真的影响到这个数组的内容
	int count(char s[100], char c){
		。。。 
	} 
	int count(char s[], char c){
		。。。 
	}	
	 
	
	拷贝字符串 
	str2 = str1; // 不行 
	str2 == str1; // 不行 	   
	
	
	cstring 相关的库
		String copy (strcpy)
		String compare (strcmp)
		Find the length of string (strlen)
		Merging strings (strcat)
		Find a character from string (strchar) 
	
	
	cin.ignore
	
	 	
	*/


//	char c;
//	cin.get(c);
	
//	char str[20]="HelloWorldHelloWorld"; // 20个元素大小的字符数组只能存19个元素 
//	char str[20]="HelloWorldHelloWorl";  // 正确 
	//  "ABC" 等同于 { 'A','B','C','\0' } 
	
//	char s[20];
//	cin.getline(s, 20); // Hello World
//	cout << s; // Hello World
//	cin >> s; // Hello World
//	cout << s; // Hello

//	char s[] = "Hello world";
//	// [Hello worl d  \0]
//	//  0123456789 10 11
//	cout << s; 
	
//	char s[]="A string! A long long long long string!";
//	cout << s << endl; 
//	s[9]='\0';
//	cout << s;

	char s[20] = "A string";
//	cout << s; 
	
	char a[20];
	strcpy(a, s);
//	cout << a; 
	
	strcat(a, s);
//	cout << a;

	// a 比 b 小 all 比 azz
//	if (strcmp(a, s) == 0) {
//		
//	}

	// 这里说的长度是不包括 '\0' 的 
	cout << strlen(a);
	 
	return 0;
}
