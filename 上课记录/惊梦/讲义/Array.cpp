#include<bits/stdc++.h>
#include<windows.h> 
using namespace std;

// global scope, 默认初始化为全 0 
double balance[5] = {3, 2, 4, 1, 0};


void swap(int &a, int &b) {
	int temp;
	temp = a;
	a = b;
	b = temp; 

//  有意思的写法，非重点 
//	a = a + b; // a = 3
//	b = a - b; // b = 3 - 1 = 2
//	a = a - b; // a = 3 - 2 = 1

	// 一个数异或本身，肯定是 0
	// 一个数异或0，还是这个数本身 
	// b = a ^ b ^ b = a ^ 0 = a
	// a = a ^ b ^ a = b ^ 0 = b
	 
//	a = a ^ b; // 10 ^ 01 = 11
//	b = a ^ b; // 11 ^ 01 = 10
//	a = a ^ b; // 11 ^ 10 = 01 
} 

int main() {
	// 数组 
	// 处理大量相同类型的（固定大小的）数据时候用到
	// 注意下标从 0 开始 
	// 注意是相同类型，如 int double
	// int 4B double 8B
	
	/*
		连续的内存位置？
		[4B=32bit][4B=32bit][4B=32bit][4B=32bit][4B=32bit]
		[4B=32bit][4B=32bit][4B=32bit][4B=32bit][4B=32bit]
		
		[1][][1]
		[1][1][]
		[1][1][1]
		[][1][1]
		[1][][1] 酒店
		如果用连续的方式安排房间，可能会找不到这么大的连续空间。
		计算机里面，“链表”。
		
		好处是什么？可以随机访问。 
		[1][1][1][1][1]				 
		因为是连续的，所以可以 “随机访问”，可以用下标访问。
		 0  1  2  3  4
		只要知道 0 号的位置，我们就可以访问任意的数组元素。
		0 号的位置保存在数组的名字中
		下标就是 从 0 号出发，往后走多少步。
		随机访问数组元素的原理
		
		下面展示了数组的元素的地址 
	*/ 
	
//	int n[ 10 ];
//	for ( int i = 0; i < 10; i++ ) {
//	  n[ i ] = i + 1; // 设置元素 i 为 i + 100
//	}
//	cout << &n << endl;    // a4 
//	cout << &n[0] << endl; // a4 
//	cout << &n[1] << endl; // a4 + 4B = a8 
// 	cout << &n[2] << endl; // a8 + 4B = ac
	
	// 声明，告诉酒店变量类型！
//	double balance[10]; // 数组数量写成是 0 个也 OK
//	cout << balance[0];  
	
	// 初始化，用大括号，里面的数组不能大于我们声明的元素数目 
//	double balance[5] = {100.0, 2.0, 3.4, 7.0, 50.0, 1.0};
//  等同于 
//  double balance[] = {100.0, 2.0, 3.4, 7.0, 50.0};
//  跟下面不同，如果您省略掉了数组的大小，数组的大小则为初始化时元素的个数。 
//  double balance[] = {100.0, 2.0, 3.4, 7.0};
 
//	const int size = 5;
//	double balance[size] = {};
	// 已初始化（收拾了，0），但是还没有安排客人入住（0） 
//	cout << balance[0]; 
	
	// Index out of bound?
	// 我们访问了超过数组长度的值
	// C++ 比 C 加了一层保护，就是这个
	// C 语言可以越界访问，给了黑客可乘之机 
	// 我们不能访问超过数组的值
	/*
		黑客程序，基本的原理 
		[1][][1][1][1][][1][1][1][][][][X][X][X] 酒店	
	*/ 
	
	// (compiler will not tell you)
	// 保存后就有一个 编译 compile 的过程， 编译失败的时候就会在代码运行之前告诉你原因
	// 访问越界不能在编译时发现
	// C++代码 -编译->  mov $1 a  -> 0101 机器语言 
							
	// Arrays declared in global scope are initialized with zeroes when the program start! 
	// 直接打印，出来的是地址 
//	cout << balance;
    // string
//	char name[] = "Alan"; 
//	if (name == "Alan") cout << "OO"; // 不能输出
//	if ("Alan" == "Alan") cout << "OO"; // 能输出
		 
	// search
//	int x, a[3];
//	for (int i=0; i<3; i++)
//	cin >> a[i];
//	cout << "Input your target: ";
//	cin >> x;
	// STL 库里面的数据对象可以用 indexOf
	 
	// Sorting
	// Bubble-Sort 冒泡排序 
	/*
		2 5 3 4 1 从小到大排序
		第一次循环：
		2 5 3 4 1 -> 2 5 3 4 1 -> 2 3 5 4 1 -> 2 3 4 5 1 -> 2 3 4 1 5 最大的排到最后面
		第二次循环： 
		2 3 4 1 5 -> 2 3 4 1 5 -> 2 3 4 1 5 -> 2 3 1 4 5
		第三次循环：
		2 3 1 4 5 -> 2 3 1 4 5 -> 2 1 3 4 5  
		第四次循环：
		2 1 3 4 5 -> 1 2 3 4 5  			
	*/
//	for(int i=0;i<5;i++){
//		cout << balance[i] << " ";
//	} 
//	cout << endl;
//	for(int i = 0; i < 4; i++) {
//		bool flag = false;
//		for(int j = 0; j < 4 - i; j++) {
//			if (balance[j] > balance[j + 1]) {
//				flag = true; 
//				swap(balance[j], balance[j+1]);
//			}
//		}
//		if (!flag) {
//			break;
//		}
//	}
//	for(int i=0;i<5;i++){
//		cout << balance[i] << " ";
//	} 
	// 优化点，如果我们发现某一次循环中，我们没有进行过swap，证明有序的了，可以停止了。 
	
	
	
	/* 上面是一维数组的内容 */ 
	
	
	/*
		多维数组 
		
		[[0,0],[0,1],[0,2],[0,3]],
		[[1,0],[1,1],[1,2],[1,3]],
		[[2,0],[],[],[]],
		[[3,0],[],[],[]]	
			
		int table[30][100];
		
		table[0][1]
		
	*/

	// 99 乘法表
//	int table[9][9];
//	for(int i = 0; i < 9; i++) {
//		for(int j = 0; j < 9; j++){
//			table[i][j] = (i + 1) * (j + 1);
//		}
//	}
//	for(int i = 0; i < 9; i++) {
//		for(int j = 0; j < 8; j++){
//			cout << setw(5) << left << table[i][j];
//		}
//		cout << table[i][8] << endl; 
//	}
	
	
	/*
		当我们在命令行手动通过指令运行程序时，以g++命令为例，实际上细分为以下步骤
		1.预处理，用g++ -E xxx.cpp > program.i表示，进行对宏，注释等的处理，生成.i文件
		2.编译, 用g++ -S program.i表示，生成.s文件，把文件转换为汇编文件
		3.汇编, g++ -c program.s，把汇编文件变成.o文件
		4.链接, g++ -O program.out program1.o program2.o … 把.o文件和库文件链接起来
		形成一个对象文件
		5.运行，使用"./program.out"即可, 需要注意的是如果在程序main函数中输出argc，
		即命令行参数个数，"./program.out"也算一个参数，即"./program.out s1 s2"这行
		命令有3个命令行参数
	*/	 
	return 0;
}
