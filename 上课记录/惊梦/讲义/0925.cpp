#include <iostream>
#include <iomanip>
#include <bits/stdc++.h> 
using namespace std;

int main() {
	// 计算机处理时间一般使用 timeStamp。 
	// 时间戳: 从1970年1月1号0点0时到现在的总秒数。
	// 1. 天/时/分/秒 --> 秒数
	// 2. 秒数 --> 天/时/分/秒 
	
	// 字符串 "For start time, please enter the following information:"
	// 用双引号引起来的。
	/*
		<< endl 换行
		cin >> startDay; 获取键盘的输入，并且赋值给 startDay
		getTimeStamp(XXXXX) 不需要记名字
		
		// 函数 
		long long getTimeStamp(int day, int hour, int minute, int second) {
			return ((day * HOURS_IN_DAY + hour) * MINUTES_IN_HOUR + minute) * SECONDS_IN_MINUTE + second;
		}
		
		// 函数的模板
		long long ：数据类型(这个函数要返回的值的数据类型，使用return来返回)
		void: 如果一个函数没有返回值，我们用void表示 
		getTimeStamp ：函数的名字
		() :  括号里面是什么？ 括号里面放的是输入参数、入参，入参也是有数据类型和名字 
		int day, int hour, int minute, int second ：我这里有四个入参
		{} ：括号里面是函数体，也就是说函数的body 
		  
		 
		// long long 是什么？int 是什么？
		// 他们都是 数据类型
		// C++的数据类型包括了 int 、 long long (8个字节) 
		// 数据类型 变量名; // int a; long long b; char c; double d; float f; short s; ...		 
		// int 这个数据类型是用来存什么的？ 整型！！！ 整数！！！ 0，1，2，3，-1，-2 ...
		// long long 是一个长整型，用来存更大的整数。
		
		// const int HOURS_IN_DAY = 24; 
		// 这个变量定义在 main 函数的外面，叫做全局变量。
		// const 在变量定义的前面加个const，意思是定义为一个常量，常量是不能被改变的。
		// constant
		// 正规的代码写法，需要把这个数字定义为常量，(magic number)
		
		// getTimeStamp(endDay, endHour, endMinute, endSecond)
		// 函数名字(需要的入参s);
		
		int seconds = timeStamp % SECONDS_IN_MINUTE;
		% 取余数，10 / 3 = 3 ... 1
		400s / 60 = 6 分钟 ... 40秒  ==>  400 % 60 
		 
		int minutes = ((timeStamp - seconds) / SECONDS_IN_MINUTE) % MINUTES_IN_HOUR;
		400 - 40 = 360 / 60 = 6 分钟 % 60 = 6 分钟
		
		int hours = (timeStamp / (MINUTES_IN_HOUR * SECONDS_IN_MINUTE)) % HOURS_IN_DAY; 
		8000s -> 8000 / 3600 = 2 总共有多少个完整的小时，通过取余，得到余下来的小时
		 
	 	int days = timeStamp / (HOURS_IN_DAY * MINUTES_IN_HOUR * SECONDS_IN_MINUTE);

		printf("The time difference is: %d day(s), %d hour(s), %d minute(s), and %d second(s).\n",
				days, hours, minutes, seconds);
		为什么没有用 cout ？
		printf 打印的方法，类似于 cout，但 printf 是 C 语言。
		
		printf("Hello"); cout << "Hello";
		%d 表示需要在这个位置加一个整数
		printf("Hello, %d", a); cout << "Hello, " << a; 
		printf("Hello, %d, %d, %d", a, b, c); cout << "Hello, " << a << ", " << b << ", " << c; 
		\n 等于 endl，换行	
		printf("Hello\n"); cout << "Hello" << endl;		
	*/ 
	
	/*
		\t Tab 4个空格 \ 反斜杠 转义字符 
		cout << "Tom:\"Hi!\""; 
	*/
//	cout << "Tom:\"Hi!\""; 

//	cout.width(10);
//	cout << 5.6 << endl;
//	cout << 5.6 << endl;
//		
//	cout << setw(5) << 1.8;
	
	// Effect is permanent
//	cout << setprecision(2) << 1.3 << endl; // 不会影响整数 
//	cout << 1.35 << endl; // 1.4 整数部分和小数部分共两位精度。
//	//  
//	cout << fixed << 100.345 << endl; 
//	
//	// printf 的写法 
//	float f = 1.345;
//	printf("%.2f\n", f);


//	string s;
//	string a;
//	cin >> s; // Hello World
//	cout << s; // Hello 空格被作为输入的分界点 --> cin.getline
//	cin >> a;
//	cout << a; 
//	return 0;

	// 注释，用 // 开头 ctrl + ？ 快捷键
	// /**/
	
	/*
		sdfsdf
		里面的都是注释 
		sdfadsf
	*/ 
	 
	 
}
