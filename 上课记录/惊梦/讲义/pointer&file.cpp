#include<iostream>
#include<algorithm>
#include<iomanip>
#include <cstring>
#include <fstream>

using namespace std;

/*
	File IO
	cin cout

	int i;

	// 新学习两个类 ifstream ofstream (工具类)

	ofstream out;

	// ifstream
	// 1. declare 声明 ifstream fin;
	// 2. 读取文件 fin.open("infile.txt");
	// 3. 把数据从 ifstream 类中读取出来，fin >> x;
	// 4. 把这个 ifstream 类删除（释放空间）， fin.close();

	// ofstream
	ofstream fout;
	fout.open("myfile.dat") ;
	fout << x;
	fout.close();

	// open 还有别的玩法，也就是 fout.open(filename, mode); fout.open("myfile.dat", ios::binary) ;
	fout.open("myfile.dat", ios::binary | ios::out) ;
	ios::in 用于输入，只读
	ios::out 用于输出，可写入
	ios::binary 二进制读取，如果没有用这个，电脑会以文本形式读取文件，会有格式转换的问题。
	ios::ate at the end, 末尾输入
	ios::app append 增添模式
	ios::trunc

	// fail
	.fail() 如果操作行为失败了（例如找不到这个文件）会返回true，否则返回false

	// exit
	如果操作行为失败时，可以调用这个函数，终止程序运行

	// eof
	文件的末尾都有一个 eof 标识，end of file。
	所以到达文件末尾的时候，eof 这个函数会返回 true
	类似于字符串最后有一个 '\0'

	// 文本文件
	fin.get() 获取一个字符
	fin.getline(char str[], size) 获取一行文本

	// 二进制文件
	fin.read(char* target, int num)



*/
int main() {

	// example 1

//	// 定义
//	ifstream fin;
//	ofstream fout;
//
//	int x, y, z;
//
//	// 这两个文件的位置位于 file.cpp (本文件) 的同一级目录
//	// 打开这两个文件，ifstream 来打开输入文件， ofstream 打开输出文件
//	fin.open("input.txt");
//	fout.open("output.txt");
//
//	// 把文件内容放到变量里面
//	fin >> x >> y >> z;
//	// 把结果放到输出文件里面
//	fout << "The sum is " << x + y + z;
//
//	fin.close();
//	fout.close();

	// example 2
//	ifstream in1;
//	in1.open("infile1.dat");
//	if (in1.fail()) {
//		cout << "Input file 1 opening failed.\n";
//		// 系统异常，退出运行，1 stands for error
//		exit(1);
//	}
//	// 因为前面已经 exit 了，所以这句话不会打印出来。
//	cout << "This is the end";


	// example 3 eof 常用用法 while (!fin.eof()) {
//	ifstream fin;
//	int x;
//	fin.open("input.txt");
//	while (!fin.eof()) {
//		fin >> x;
//		cout << x << " ";
//	}

	// 指针 Pointer
	// 变量的地址  加&
	int var1 = 666;
//	cout << &var1;  // 0x28fecc

	// 指针是什么？指针也是一个变量，它的值是一个地址。
	// 存储地址的变量叫做指针。 

	// 定义一个指针, 多了一个*
	int* varPtr1;
	// 把var1的地址存储在varPtr2中
	int* varPtr2 = &var1;


	// 通过地址也可以找到该地址所保存的变量，也就是找到那个存储在这个地址的变量 
	// 通过在指针变量前面加 *，就可以取到该地址所保存的变量
//	cout << *varPtr2 << endl; // 666
//	cout << &varPtr2 << endl; 
	/*
		变量名	  var1(int) 	varPtr2(指针)
		地址      0x28fecc   	0x28fec8(指针变量也是变量，所以也会有地址) 
		保存的值  666			0x28fecc
	*/
	
	// 数组 
	int arr1[3] = {1, 2, 3};
	// arr1 数组的变量名，其实也是一个指针变量，因为它存储的也是地址。 
//	cout << arr1; // 0x28febc
	int* arr2; 
	arr2 = arr1;
	
	for(int i = 0; i < 3; i++){
		cout << *arr2 << endl;
		// 指针移动到下一个位置 
		arr2++; 
	}
	
	return 0;
}
