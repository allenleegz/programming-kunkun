#include<iostream>
using namespace std;


void q1() {
	cout << "Enter the value of A, B and C:" << endl;
	int a, b, c;
	cin >> a >> b >> c;
	
	// Impossible Case
	if (a <= 0 || b <= 0 || c <= 0 || a + b <= c
		|| a + c <= b || c + b <= a) {
		cout << "Impossible" << endl;	
	} else if (a == b && b == c) { 
		cout << "Equilateral" << endl;
	} else if (a == b || b == c || a == c) {
		cout << "Isosceles" << endl;
	} else {
		cout << "Scalene" << endl;
	}	
}

// void 表示返回的类型，表示不返回任何值 
void q2() {
	cout << "Enter an Integer Number: ";

	int a;
	cin >> a;

	
	if (a % 3 == 0) {
		if (a % 5 == 0) {
			cout << a << " is divisible by 3 and 5." << endl;
		} else {
			cout << a << " is divisible by 3 only."	<< endl;
		} 
	} else {
		if (a % 5 == 0) {
			cout << a << " is divisible by 5 only." << endl;
		} else {
			cout << a << " is not divisible by 3 or 5."	<< endl;
		} 
	}	 
}

int main() {
	// ctrl + s 保存 
	
	while(1){
		q2();
	}
	return 0;
}
