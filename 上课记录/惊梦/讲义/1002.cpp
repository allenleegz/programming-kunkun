#include<bits/stdc++.h>
using namespace std;
int main() {
	/* 
	1. 变量名字 不能有空格，如 math book、MathBook 
	2. 变量名字 要用小写开头，驼峰型式, mathBook 
	*/ 
	
	// 输出 N 位小数 
//	cout << fixed << setprecision(N) << XXX;
	
	// 怎么判断的数是不是int呢?
	// ! 取反 
//	int a, b;
//	cin >> a >> b;
//	if(!cin.good()) {
//		cout << "error" << endl;
//	} else {
//		cout << "good" << endl;
//	}

	// printf %d 用来输出整数 %s 字符串 %c 字符 %f 小数 
	// \n 换行 类似 endl 
//	printf("Hello+World"); 
//	printf("%d+%d=%d\n", a, b, a+b); 
	
	// / 如果两边都是整数，那么这个就是整除 ，10 / 6 = 1  
//	(double)a/b
	
	// 不要在switch里面定义变量
	
	// 双目运算符
	// 布尔值 ? XXX : YYY; 
//	s = a < b ? "T" : "F";  

//	如果用printf输出字符串string，XXX.c_str()


	// 三种写法：for、 while、 do while
	// for ( initialization ; comparison ; adjustment ) statement; 
//	for ( 初始化语句; 条件语句; 每次循环结束时执行的语句) {
//	   statement(s);
//	   statement(s);
//	   statement(s);
//	   statement(s);
//	   statement(s);
//	   statement(s);
//	   statement(s);
//	} 
	
	// 经典写法，i 其实是 index，0 -> 3(不执行) ，也就是说 for 循环运行了3次？ 
//	for (int i = 0; i < 3; i = i + 1 ) {
//	   cout << i << endl;
//	}
//		
//	初始化语句：可以不写。循环开始前会且只会执行一次，如果在这里定义一个变量，这个变量的作用范围是在for
//	循环的{}内生效，超出这个{}就不生效。因此，初始化的 i 不会影响其他的地方。
//	
//	条件语句：可以不写。该条件满足的时候，也就是等于true，{}里面的代码才会执行。如果不等于true，
//	循环就到此终止。执行到循环的代码的时候，首先就会先判断一次条件是否满足。 
//	
//	每次循环结束时执行的语句: 可以不写。每次循环结束都会执行一次，注意没有; 
//	
//	即使有某个语句不写，; 还是需要保留的。 

//	int i = 666;
//
////	i = i + 1;
////	cout << i; // 2
//	
//	// 此时，在这个for循环内部用的i是他内部定义的i 
//	for (int i = 0; i < 3; i = i + 1 ) {
//	   cout << i << endl; // 0 1 2 
//	}
//	
//	cout << i; // 内部的i已经被回收了，所以此处用的是外部的i，即 666 


	int i = 6;
	
	// 此时，在这个for循环内部用的i是？？？
	// 用的就是外部的i
	// 如果内部有定义就用内部的，否则就用外部的。
	// 只有内部定义的变量，循环结束后会被回收。 
	// 这里的i是在外部定义的，因此循环结束后也不会被回收。 
	for (; i < 10; i = i + 1 ) {
	   cout << i << endl; // 6，7，8，9  
	}
	
	cout << i; // ？10
	
	return 0;
}
