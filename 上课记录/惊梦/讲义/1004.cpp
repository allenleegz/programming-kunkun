#include<bits/stdc++.h>
#include<windows.h> 
using namespace std;
int main() {
	// 10/04 早上课程，讲解了 A1 作业的答案，A1 非常重要！里面的相关知识点一定要看懂
	
	// 10/04 晚上课程
//	    初始化  是否进入循环 每次循环后执行一次 
//	for(int i = 0; i <= 10; i++){
//		cout << i << endl;
//	}

	// 整除
	// 能被 3 整除
	// % 3 == 0 ：% 是取余符号，a % b，意思是求 a 被 b 整除后得到的余数。10 / 3 = 3 ...... 1， 10 % 3 = 1 
	// 我们可以用取余运算计算整除 
	// printf %d %s %c ... 不是一个符号只有一个意思的，这里表示占位符，是在 printf 里面会使用的。 

/*	 
	// how to check if the number is a prime number
	int num;
	cin >> num;
	
	// 常用写法，先设置为 true，如果在循环中发现了某个东西，就设为false 
	bool flag = true;
	// 为什么从 2 开始？为什么不是 0 ？因为素数只能被 1 和自身整除
	// 不能除以 0，1 本来就能整除 num
	// 所以从 2 开始，只要有除了1和num以外的数字可以整除 num，那么 num 就不是素数 
	
	// 遍历（就是指的for循环，从某个数一直尝试）到哪个数？
	// 一个数不可能被大于他自身一半的数字整除
	 
	for(int i = 2; i < num / 2; i++){
		if (num % i == 0) {
			flag = false;
			break; // 停止循环，跳出循环。 
		}
	}
	
	if (flag == true) {
		cout << "is prime number";
	} else {
		cout << "is not prime number";
	}
*/

	// 除非考试要求用 while，否则 for 可以完成所有 while 做的事情。
	// 条件判断式 == true 时，也就是非 0，就会执行循环体的内容 
//	while(条件判断式) {
//		循环体	
//	}


//	int a = 10;
//	while(a >= 0) {
//		cout << "熊猫烧香" << endl; // 打印了11次 
//		Sleep(1000); // 睡眠 1000 ms
//		a--;
//	}
//	cout << a << endl; // ？？？-1 

	// 先执行一次，执行完之后检查循环条件是否为 true
 
//	int a = 0;
//	do
//	{
//	   cout << "Hello";
//	
//	} while(a > 0);	


	// break 和 continue 
//	break    直接终止循环
//	continue 停止当前的循环，跳到下一次循环 
	 
	
	
	// 嵌套循环
//	for(int i = 1; i <= 10; i++){
//		if (i == 7) continue; // continue 后，下面的代码就不执行了。 
//		cout << "i: " << i << endl; 
//	} 
	 
	// 输出偶数, 2 - 20
//	for(int i=2;i<=20;i+=2){
//		cout << i << endl;
//	}

// 取余，取模，mod 
// 5 / 2 = 2 ... 1 
//	for(int i=2;i<=20;i++){
//		if (i % 2 == 1) continue; 
//		cout << i << endl;
//	}

	// Nested Loop 嵌套循环 
	// 循环使用的index索引不能用同一个变量
	// "" 内的都是字符串 
//	for(int i=0;i<5;i++){
//		cout << "i:" << i << endl;
//		for(int j=0;j<5;j++){
//			cout << "	j:" << j << endl;  
//		}
//	}

//	for(int i=0;i<5;i++){
//		cout << "i:" << i << endl;
//		for(int j=0;j<5;j++){
//			cout << "	j:" << j << endl;
//			break;
//		}
//	}

	// 找质数
//	for(int i = 2;i <= 100; i++){
//		// 初始化一个布尔值，如果我们找到了一个能整除的数，那么这个flag就被设为false
//		 
//		bool flag = true;
//		// 遍历除数，从2到i/2 
//		// 一个数的因数（能整除它的数）肯定小于这个数的一半 
//		// 66 = 33*2 34 35 36  
//		for(int j = 2;j < i/2; j++){
//			if(i % j == 0) {
//				flag = false;
//				break;
//			}
//		}
//		if (flag) {
//			cout << i << "是质数。" << endl;
//		}
//	}

	// 尽量不要用浮点数（小数、float、double）作为循环的控制条件
	// 因为浮点数是不精准的
	
	// 输出一个 m * n  
//	int m, n;
//	cin >> m >> n;
//	int cnt = 1;
//	for(int i=0;i<n;i++){
//		for(int j=0;j<m-1;j++){
//			cout << cnt << ",";
//			cnt++;
//		}
//		cout << cnt;
//		cnt++;
//		cout << endl;
//	}
	 
	return 0;
}
