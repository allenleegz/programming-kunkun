#include <bits/stdc++.h>
#include <fstream>
using namespace std;

// C++ File 2 节课 
int main()
{
	// 1. What file in C++ / Computer?
	/*
		File word, video, cpp
		directory manage files tree
		file stored in computer 0101010101 Bit String
		.txt .cpp
	*/
	
	// 2. ifstream, ofstream, fstream
	// file operate class
	
	// fstream
	// char *  表示字符指针、字符串 
    const char *desc ="Hello Tommy!";
    
	//create fstream class 
//    fstream fs;
    
    // open "Tommy.txt" if no then new one file.
    // ios::out input to the file 
//    fs.open("./Tommy.txt", ios::out);
    
    // write into the file
//    fs.write(desc, 10);
//    fs.write(desc, 10);
    
    // remember to close , memory leak
//    fs.close();	 
 	
	// 1. open
	// specify a file name
	// specify usage read-only write-only read-write
	
	 
//    ifstream inFile;
    // try to open a file dont whether it exists
    // file not case sensative
	// ios::in only use to read data, if file not exist, return false
//    inFile.open("tommy.txt", ios::in);
//    if (inFile)
//        inFile.close();
//    else
//        cout << "test.txt doesn't exist" << endl;
        
        
	// ios::out open file and write data
	// if file not exists then create
	// if file exists then overwrite
//    ofstream oFile;
//    oFile.open("test1.txt", ios::app);
//    if (!oFile)  //条件成立，则说明文件打开出错
//        cout << "error 1" << endl;
//    else {
//		oFile.write(desc, 20);
//        oFile.close();
//	}
    	
    
    /*
		why \\ ?  \ 转义字符 \\ 表示是一个 \  
	*/ 
//	oFile.open("tmp\\test2.txt", ios::out | ios::in);
//    if (oFile)  //条件成立，则说明文件打开成功
//        oFile.close();
//    else
//        cout << "error 2" << endl;


	// ios::trunc 
//    fstream ioFile;
//    ioFile.open("..\\test3.txt", ios::out | ios::in | ios::trunc);
//    if (!ioFile)
//        cout << "error 3" << endl;
//    else
//        ioFile.close();
	
	
	// another way, most commonly used
//	ifstream inFile("test1.txt", ios::in);
//	if (inFile)
//        inFile.close();
//    else
//        cout << "test.txt doesn't exist" << endl;
//    ofstream oFile("test1.txt", ios::out);
//    if (!oFile)
//        cout << "error 1";
//    else
//        oFile.close();
//    fstream oFile2("tmp\\test2.txt", ios::out | ios::in);
//    if (!oFile2)
//        cout << "error 2";
//    else
//        oFile.close();

//   char data[100];
// 
//   // 以写模式打开文件
//   ofstream outfile;
//   outfile.open("afile.dat");
// 
//   cout << "Writing to the file" << endl;
//   cout << "Enter your name: "; 
//   cin.getline(data, 100);
// 
//   // 向文件写入用户输入的数据
//   outfile << data << endl;
// 
//   cout << "Enter your age: "; 
//   cin >> data;
//   cin.ignore();
//   
//   // 再次向文件写入用户输入的数据
//   outfile << data << endl;
// 
//   // 关闭打开的文件
//   outfile.close();
// 
//   // 以读模式打开文件
//   ifstream infile; 
//   infile.open("afile.dat"); 
// 
//   cout << "Reading from the file" << endl; 
//   infile >> data; 
//  
//   // 在屏幕上写入数据
//   cout << data << endl;
//   
//   // 再次从文件读取数据，并显示它
//   infile >> data; 
//   cout << data << endl; 
// 
//   // 关闭打开的文件
//   infile.close();
   	
//	// getline 读取一行 string  
//    char c[40];
//    //以二进制模式打开 in.txt 文件
//    ifstream inFile("in.txt", ios::in | ios::binary);
//    //判断文件是否正常打开
//    if (!inFile) {
//        cout << "error" << endl;
//        return 0;
//    }
//    
//    int avg_age = 0;
//    
//    for(int i = 0; i < 4; i++){
//		inFile.getline(c, 40);
//		char *p;
//		const char *d = " ";
//		p = strtok(c, d);
//		
//		int counter = 1;
//		while(p) {
//			if (counter == 2) {
//				avg_age += stoi(p);
//			}
//			p=strtok(NULL, d);
//			counter++;
//			counter = counter % 3;
//		}
//	}
//    
//    cout << "AVG AGE: " << avg_age / 4.0 << endl;
//    inFile.close();
//    return 0;
}
