#include <bits/stdc++.h>
using namespace std;

// class_name begin with upper-class letter
// object_names is an optional list
// access specifiers :  private, public or protected.

// private��or from their "friends" 
// protected: also from members of their derived classes.
// By default��have private access for all its members.


//class Rectangle {
//    int width, height;
//  public:
//    void set_values (int, int);
//    // has been included directly within the definition of class 
//	// Rectangle given its extreme simplicity.
//	//  the constructor that takes no parameters,
//    int area() {return width * height;}
//    //  the default constructor 
//	Rectangle ();
//	Rectangle (int);
//    Rectangle (int,int);
//};
//
//Rectangle::Rectangle (int a) {
//  width = a;
//}
//
//Rectangle::Rectangle (int a, int b) {
//  width = a;
//  height = b;
//}
//
////Rectangle::Rectangle (int x, int y) : width(x) { height=y; }
////Rectangle::Rectangle (int x, int y) : width(x), height(y) {}
//	 
//
///*
//	scope operator ::
//	Rectangle:: need add classname to the function 
//*/
//void Rectangle::set_values (int x, int y) {
//  width = x;
//  height = y;
//}

// overloading operators example
#include <iostream>
using namespace std;

//class CVector {
//  public:
//  	static int n;
//    int x, y;
//    CVector () {};
//    CVector (int a,int b) : x(a), y(b) {}
////    CVector operator + (const CVector&);
//};

// const can not change the value of input param
//  
//CVector CVector::operator+ (const CVector& param) {
//	/*
//		temp = {x= 0, y= 4236496}
//		this = 0x28fec8
//		param = @0x28fec0: {x= 1, y= 2}
//	*/
//  CVector temp;
//  temp.x = x + param.x;
//  temp.y = y + param.y;
//  return temp;
//}

//CVector operator+ (const CVector& lhs, const CVector& rhs) {
//  CVector temp;
//  temp.x = lhs.x + rhs.x;
//  temp.y = lhs.y + rhs.y;
//  return temp;
//}

class Dummy {
  public:
  	// there is only one common variable for all the objects of that same class
    static int counter;
    Dummy () { counter++; };
};

int Dummy::counter = 0;

int main()
{
	// object vs class
	// object need some space in computer to store
//	Rectangle rect;
//	rect.set_values(3, 4);
//	cout << "area: " << rect.area() << endl;

	// Constructors
	// a special function
//	Rectangle rect(3, 4);
	// Rectangle rectc(); // oops, default constructor NOT called
//	cout << "rect area: " << rect.area() << endl;
	// Constructors cannot be called explicitly
	
	// Overload
	
	
	// uniform initialization
	// constructors with a single parameter 
    // four ways to construct objects of a class whose constructor takes a single parameter
	
//	Rectangle r1 (10);
//	Rectangle r2 = 10;
//	Rectangle r3 {10};
//	Rectangle r4 = {40}; 
	
	// using member initialization
	
//	Rectangle obj (3, 4);
//	Rectangle * foo, * baz;
//	baz = new Rectangle[2] { {2,5}, {3,6} };
//	
//	foo = &obj; 
//	cout << "*foo's area: " << foo->area() << '\n'; 


//	CVector foo (3,1);
//	CVector bar (1,2);
//	CVector result;
//	result = foo + bar;
//	cout << result.x << ',' << result.y << '\n';
	
	// overloaded as non-member functions
	
	// this
	// a pointer to the object whose member function is being executed

//	CVector foo (3,1);
//	CVector bar (1,2);
//	
//	foo = bar;

	// Static member
	// A static data member of a class is also known as a "class variable"
//	Dummy d;
//	cout << d.counter << endl;
//	cout << Dummy::counter << endl;
//	// array 
//	Dummy b[5];
//	cout << Dummy::counter << endl;
//	// dynamic generation
//	Dummy * c = new Dummy;
//	cout << Dummy::counter << endl;
//	// manually release 
//	delete c;
	
	cout << Dummy::counter << endl;
	
	// Because they are like non-member functions, they cannot access non-static members of the class
	// static no need to create an object
	// non-member functions, can be invoke without any object
	// non-member function so, can not access any member variable.
	// static function can only use static member
	 
	return 0;
}
