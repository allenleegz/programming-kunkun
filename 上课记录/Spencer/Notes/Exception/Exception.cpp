#include <bits/stdc++.h>
#include <fstream>
using namespace std;


/*

try-catch

try {
	// C++ code 
} catch (exceptionType[exception] variable[e]) 

exception has many types

throw ��directly throw an exception

exception type in C++��
	1. int��char��float��bool and so on, basic data type;
	2. string��array��pointer��class��struct
	3. C++ exception class 

*/ 


void fun() {

//	int i;
//	cin >> i; 
		
//	string str = "http://c.biancheng.net";
	// Index out of bound

//  not convenient  
//	if (i < 20) {
//		//TODO
//	} 
//    char ch1 = str[90]; 
//    cout<<ch1<<endl;
    
    
//    try {
//		// Index out of bound��throw exception 
//	    char ch2 = str.at(i);  
//	    cout<<ch2<<endl;
//									
//	} catch (exception &e) {
//		cout << "Input error, please try again." << endl;
//	} catch ()


//	try {
//		throw "Unknown Exception";
//	} catch (const char* &e) {
//		cout << e;
//	} 
//	

//    throw "Unknown Exception";  //�׳��쳣
//    cout<<"[1]This statement will not be executed."<<endl;

}

class Base{ };

class Derived: public Base{ };


int main()
{
//	try {
//		fun();
//		
//		cout<<"[2]This statement will not be executed."<<endl;
//	} catch (const char* &e){
//		cout<<e<<endl;
//	}
	
//	while(1){
//		fun();
//	}

//	char s[10] = "SSS";

//    try{
//    	int i;
//    	cin >> i;
//    	switch (i) {
//			case 1:
//				throw Derived();
//				break;
//			case 2:
//				throw 666;
//				break;
//			case 3:
//				// const
//				throw "666";
//				break;
//			case 4:
//				throw Base();
//				break;								
//		}
//        
//        cout<<"This statement will not be executed."<<endl;
//    }catch(float){
//        cout<<"Exception type: int"<<endl;
//    }catch(const char *){
//        cout<<"Exception type: char *"<<endl;
//    }
//    catch(Derived){
//        cout<<"Exception type: Derived"<<endl;
//    }
//    // child class can be upper transfer back to father class
//    // father class can not be tranfer to child class
//	// father class can receive child class exception
//	catch(Base){
//        cout<<"Exception type: Base"<<endl;
//    }
	
	
	
}
