#include<iostream>
#include<algorithm>
#include<iomanip>
#include<cmath>


// ctrl + A 全选后，ctrl + shift + A 格式化
using namespace std;

// 类的定义
class Triangle {
	private:
		double side1, side2, side3;
		double area;
	public:
		Triangle() {
			side1 = side2 = side3 = area = 0;
		}

		Triangle(double side1, double side2, double side3) {
			setSides(side1, side2, side3);
			area = 0;
		}

		void setSides(double side1, double side2, double side3) {
			this->side1 = side1;
			this->side2 = side2;
			this->side3 = side3;
		}

		double getArea() {
			double s = (side1 + side2 + side3) / 2;
			area = sqrt(s * (s - side1) * (s - side2) * (s - side3));
			return area;
		}

};

int main() {

	Triangle arr[10];

	cout << "Enter a number between 1 and 10:\n";
	int n;
	cin >> n;

	for (int i = 1; i <= n; i++) {

		cout << "Enter the sides of triangle " << i << ":" << endl;
		double side1, side2, side3;
		cin >> side1 >> side2 >> side3;

		Triangle triangle(side1, side2, side3);
		arr[i - 1] = triangle;

	}

	double max = 0;
	int maxIndex = 0;

	for (int i = 0; i < n; i++) {

		double area = arr[i].getArea();

		printf("Area of triangle %d: ", i + 1);
		cout << setprecision(2) << fixed << area << endl;

		if (area > max) {
			maxIndex = i;
			max = area;
		}
	}

	/*
		printf(a); 等于 cout << a;

		printf("Hello");  等于 cout << "Hello";

		int a = 666;
		printf("Hello %d", a);  等于 cout << "Hello " << a;

		double f = 6.66;
		printf("Hello %.2f", f);
		等于 cout << setprecision(2) << fixed << "Hello " << f;
	*/

	printf("Triangle %d has the largest area: %.2f\n", maxIndex + 1, max);

	return 0;
}
