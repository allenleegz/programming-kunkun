#include<iostream>
#include<iomanip>
using namespace std;

int main() {
	// memory [11110000][][][] int i; // 4B
	// new delete
	// pointer  
	
	/*
		int b;
		
		name  address   value
		  b   0x28fecc  666
		bPtr  0x28fec8  0x28fecc   
	*/
//	int b = 666;
//	cout << &b << endl; // address of b
//	int* bPtr = &b; // the value of pointer is address
//	cout << &bPtr << endl;
//	cout << bPtr << endl;


	// use `new` to create an array
	int n;
	cin >> n;
	int* b;
	b = new int [n];
	cin >> b[n-1];
	cout << b[n-1];
	
	return 0;
}
